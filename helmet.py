#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals
import codecs

import pyperclip
import os, sys
import scrypt
import json
import math

# because python like diversity
if sys.version_info < (3,) and sys.platform.startswith('win'):
    from getpass import getpass as _getpass
    def getpass(s):
        try:
            return _getpass(str(s))
        except UnicodeEncodeError:
            from locale import getpreferredencoding
            try:
                return _getpass(s.encode(getpreferredencoding()))
            except UnicodeEncodeError:
                return _getpass(b'Master password: ')
else:
    from getpass import getpass

try:
    input = raw_input
except NameError:
    pass


def generate(master_password, keyword, cost, oLen=0):
    hashed = scrypt.hash(password = master_password, salt = keyword, N = 1 << cost, buflen=64)
    
    if oLen:
        return codecs.encode(hashed, 'hex').decode('utf-8')[0:oLen]

    return codecs.encode(hashed, 'hex').decode('utf-8')


def generate_readable(master_password, keyword, lang, cost, num_words=None):
    if not num_words:
        num_words = params['nwords']
    
    wordlist = '%s/wordlist_%s.db' % (path, lang) 
    with open(wordlist, 'rb') as f:
        words = f.read().splitlines()

    dict_len = len(words)
    
    entropy_per_word = math.log(dict_len, 2)
    
    hash = generate(master_password, keyword, cost)
    
    available_entropy = len(hash) * 4
    
    hash = int(hash, 16)
    
    if (num_words * entropy_per_word) > available_entropy:
        raise Exception("Specified hashfunc (%d) is very small." % available_entropy)
    
    phrase = []
    
    for i in range(num_words):
        remainder = hash % dict_len
        hash = hash // dict_len
        phrase.append(words[int(remainder)].strip().decode('utf-8'))

    return " ".join(phrase).lower().capitalize()


def copy_to_clipboard(generated):
    global copied

    selection = safe_input('Which password would you like to copy? (1/2) ').strip()
    
    if selection == '1':
        password = generated[0]
    elif selection == '2':
        password = generated[1]
    else:
        print(err('Invalid option. Pick either 1 or 2.\n'))
        return copy_to_clipboard(generated)
    
    try:
        pyperclip.copy(password)
        copied = True
        print('\nCopied!\n')
        return
    except pyperclip.exceptions.PyperclipException:
        print(err('Could not copy! If you\'re on a Linux OS, make sure you have xclip ...\n'))


def err(text):
    # maybe app log can implemented
    return '%s' % (text)


def settings(text):
    # maybe app log can implemented
    return '%s' % (text)


def password(text):
    # maybe app log can implemented
    return '%s' % (text)


def safe_input(string):
    # maybe app log can implemented
    try:
        return str(input(string))
    except EOFError:
        print(err('Input unusable.\n'))
        return safe_input(string)


def exit_protocol(msg=''):
    if copied:
        pyperclip.copy('')
    if msg:
        print(err('\n' + msg))
    print(err('\nExiting securely... You may close this terminal.'))
    raise SystemExit


def get_defaults():
    print('\nEnter your preferred settings: (leave blank to accept defaults)\n')
    
    lang = safe_input('Select a word list EN or TR [default=en]: ')
    
    if lang:
        if lang == 'tr' or lang == 'TR':
            oLang = 'tr'
        else:
            oLang = 'en'

    cost = safe_input('Cost factor as a power of 2 [default=14]: ')

    if cost:
        if cost.isdigit():
            cost = int(cost)
            if cost < 10 or cost > 24:
                print(err('Input must be a positive integer between 10 and 24.\n'))
                return get_defaults()
        else:
            print(err('Input must be a positive integer between 10 and 24.\n'))
            return get_defaults()
    else:
        cost = 14

    oLen = safe_input('Length of generated passwords [default=32]: ')

    if oLen:
        if oLen.isdigit():
            oLen = int(oLen)
            if oLen > 64 or oLen < 16:
                print(err('Input must be a positive integer between 16 and 64.\n'))
                return get_defaults()
        else:
            print(err('Input must be a positive integer between 16 and 64.\n'))
            return get_defaults()
    else:
        oLen = 32

    nwords = safe_input('Number of words in a readable password [default=6]: ')

    if nwords:
        if nwords.isdigit():
            nwords = int(nwords)
            if nwords > 16 or oLen < 4:
                print(err('Input must be a positive integer between 3 and 16.\n'))
                return get_defaults()
        else:
            print(err('Input must be a positive integer between 3 and 16.\n'))
            return get_defaults()
    else:
        nwords = 6

    print() # line break for formatting

    return {"oLang" : oLang, "cost" : cost, "oLen" : oLen, "nwords" : nwords}


def getPath():
    try:
        return os.path.dirname(os.path.abspath(__file__))
    except:
        exit_protocol('ERROR: Cannot get path. Are you sure you\'re not running Visionary from IDLE?')


def getConfig():
    try:
        with open('%s/helmet.cnf' % path) as f:
            config = json.loads(f.read().strip())
        if config['oLen'] == 'en' or config['oLen'] == 'tr' or config['oLen'] < 16 or config['oLen'] > 64 or config['cost'] < 10 or config['cost'] > 24 or config['nwords'] > 16 or config['nwords'] < 3:
            exit_protocol('Invalid config! Please delete the configuration file (%s) and a new one will be generated on the next run.' % (path + '/helmet.cnf'))
        return config, 0
    except IOError:
        config = get_defaults()
        autosave = safe_input('Do you want to save this config? (Y/n) ').lower()
        if autosave == 'yes' or autosave == 'y' or autosave == '':
            print('\nAutosaving configuration...')
            try:
                with open('%s/helmet.cnf' % path, 'a') as f:
                    f.write(json.dumps(config))
                return config, 0
            except:
                print(err('Autosaving failed! (Permission denied)\n'))
                print('In order to save these settings, place %s' % settings(json.dumps(config)))
                print('in %s' % (settings('%s/helmet.cnf' % path)))
        return config, 1
    except KeyError:
        exit_protocol('Invalid config! Please delete the configuration file (%s) and a new one will be generated on the next run.' % (path + '/helmet.cnf'))


# Global parameters
params = {}
path = getPath()
copied = False

def interactive(first_run=True):
    if first_run == True:
        print("""
    __  __     __               __ 
   / / / /__  / /___ ___  ___  / /_
  / /_/ / _ \/ / __ `__ \/ _ \/ __/
 / __  /  __/ / / / / / /  __/ /_  
/_/ /_/\___/_/_/ /_/ /_/\___/\__/  
HELMET - No Database Password Manager
Please report any issues at info@bilims.com
-------------------------------------------""")

        global params
        params, stat = getConfig()

        if stat == 0:
            print("""
[+] Wordlist language: %s                
[+] Cost factor: 2^%s
[+] Length of standart password: %s
[+] Words in readable password: %s
[+] Config file: %s""" % (settings(params['oLang']),
                          settings(params['cost']),
                          settings(params['oLen']),
                          settings(params['nwords']),
                          settings('%s/helmet.cnf' % path)))
    
    print() # line break for formatting

    master_password = getpass('Master password: ')

    if len(master_password) >= 8:
        # Fingerprint confirms to the user that they entered the correct master password.
        
        wordlist = '%s/wordlist_%s.db' % (path, settings(params['oLang'])) 
        with open(wordlist, 'rb') as f:
            words = f.read().splitlines()

        fingerprint = generate_readable(master_password,
                                        b'',
                                        params['oLang'],
                                        params['cost'],
                                        5)
        print('Fingerprint: %s\n' % settings(fingerprint))
        while True:
            keyword = safe_input('Keyword: ')
            if keyword:
                # Generate password
                conventional = generate(master_password,
                                        keyword,
                                        params['cost'],
                                        params['oLen'])
                readable = generate_readable(master_password,
                                             keyword,
                                             params['oLang'],
                                             params['cost'])
                generated = [conventional, readable]
                print('\n[%s] Standart password: %s' % (settings('1'), password(generated[0])))
                print('[%s] Readable password: %s\n' % (settings('2'), password(generated[1])))
                # Copy to clipboard
                confirm = safe_input('Would you like to copy a password to the clipboard? (Y/n) ').lower().strip()
                if confirm == 'yes' or confirm == 'y' or confirm == '':
                    copy_to_clipboard(generated)
                else:
                    print() # line break for formatting
            else:
                exit_protocol()
                raise SystemExit
    else:
        print(err('Password must be at least 8 characters.'))
        interactive(False)

def __init__():
    init()

def main():
    try:
        interactive()
    except KeyboardInterrupt:
        exit_protocol('\nKeyboard Interrupt')
    except Exception as e:
        exit_protocol('ERROR: %s\n\nPlease report this error at info@bilims.com' % str(e))


if __name__ == "__main__":
    main()